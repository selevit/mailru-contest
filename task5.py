# coding: utf-8
#
# По условия задачи, строка должна занимать 100Gb
# У нас в наличии есть 128 гигабайт памяти
# Следовательно, мы должны эту строку сортировать на месте,
# без копирования в другую строку (использовать возврат значения функцией)
#
# В Python нельзя (по крайней мере человеческими способами)
# передавать простые строки по ссылке в функцию
# Можно было определить строку глобальной переменной и сразу ее
# отсортировать, но это не очень красиво будет выглядеть
# и не совсем в духе Python
#
# Для упрощения примера я поместил символы строки в список
# и передаю этот список в функцию сортировки
#
# По памяти сложность быстрой сортировки равна O(lg n)
# Следовательно, нам потребуется чуть менее 7 гигабайт дополнительной
# памяти для сортировки строки 100 Гб. Следовательно, 128 Гб вполне хватит
#
# P.S. Само собой, в реальном коде я бы использовал list.sort() :-)

import random


def qsort_inplace(lst, start=0, end=None):
    """
    Отсортировать список методом быстрой сортировки
    """

    def subpart(lst, start, end, pivot_index):
        lst[start], lst[pivot_index] = lst[pivot_index], lst[start]
        pivot = lst[start]
        x = start + 1
        y = start + 1

        while y <= end:
            if lst[y] <= pivot:
                lst[y], lst[x] = lst[x], lst[y]
                x += 1
            y += 1

        lst[start], lst[x - 1] = lst[x - 1], lst[start]
        return x - 1

    if end is None:
        end = len(lst) - 1
    if end - start < 1:
        return

    pivot_index = random.randint(start, end)
    x = subpart(lst, start, end, pivot_index)
    qsort_inplace(lst, start, x - 1)
    qsort_inplace(lst, x + 1, end)


if __name__ == "__main__":
    test_string = []
    for _ in range(100):
        for c in range(ord('0'), ord('9')):
            test_string.append(chr(c))
    random.shuffle(test_string)
    qsort_inplace(test_string)
    print(test_string)
