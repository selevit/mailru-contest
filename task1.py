# coding: utf-8


def remove_event_items(val):
    """
    Удалить элементы с четными индексами из списка
    """

    return val[1::2]


def reverse_list(val):
    """
    Вернуть список в обратном порядке
    """

    x = len(val) - 1
    result = []
    while x >= 0:
        result.append(val[x])
        x -= 1
    return result


if __name__ == "__main__":
    print "Removing event items..."
    print(remove_event_items(range(1000)))
    print "Sorting list..."
    print(reverse_list(range(1000)))
