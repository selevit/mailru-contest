# coding: utf-8


def print_call_count(func):
    """
    Посчитать количество вызовов функции и вывести в STD
    """

    def wrapped(*args, **kwargs):
        wrapped.counter += 1
        print "function called %d times" % wrapped.counter
        return func(*args, **kwargs)

    wrapped.counter = 0

    return wrapped


@print_call_count
def decorated_func(x):
    """
    Функция для теста декоратора для подсчета вызова функции
    """

    return x


if __name__ == "__main__":
    for x in range(10):
        decorated_func(x)
