SELECT id, name, last_name FROM Employee where last_name IN (
   SELECT last_name from Employee group by last_name having count(*) > 1
);
